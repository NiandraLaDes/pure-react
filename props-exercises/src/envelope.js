import React from 'react';
import PropTypes from 'prop-types';

export var fromPerson = {
    name: 'Bart van Enter',
    address: {
        zipcode: '1403AP',
        street: 'Amersfoortsestraatweg 85A',
        city: 'Bussum'
    }
}

export var toPerson = {
    name: 'Mr. & Mrs. van Enter',
    address: {
        zipcode: '1749EH',
        street: 'Stationsstraat 8',
        city: 'Warmenhuizen'
    }
}

const AddressLabel = ({person}) => {
    const {name, address} = person;
    return (
        <div className="label">
            <div className="name">{name}</div>
            <div className="street">{address.street}</div>
            <div className="zipcode-city">{address.zipcode} {address.city}</div>
        </div>
    )
}

const Stamp = ({amount}) => (
    <div className="stamp">{amount}</div>
)

export const Envelope = ({to, from, stamp}) => (
    <div className="envelope">
        <div className="from-stamp">
            <div className="from">
                <AddressLabel person={from}/>
            </div>
            <Stamp amount={stamp} />
        </div>
        <div className="to">
            <AddressLabel person={to}/>
        </div>
    </div>
)

Envelope.propTypes = {
    to: PropTypes.object.isRequired,
    from: PropTypes.object.isRequired,
    stamp: PropTypes.number.isRequired,
}

AddressLabel.propTypes = {
    person: PropTypes.shape({
        name: PropTypes.string.isRequired,
        address: PropTypes.shape({
            street: PropTypes.string.isRequired,
            zipcode: PropTypes.string.isRequired,
            city: PropTypes.string.isRequired
        }).isRequired
    }).isRequired
}

Stamp.propTypes = {
    amount: PropTypes.number.isRequired
}
