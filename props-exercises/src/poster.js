import React from 'react';
import PropTypes from 'prop-types';

export var posterContent = {
    image: 'https://reactjs.org/logo-og.png',
    title: 'React',
    text: 'React is a great javascript library'
}

export const Poster = ({content}) => {
    const {image, title, text} = content
        return (
            <div className="poster">
                <img src={image} alt={title}/>
                <h1>{title}</h1>
                <p>{text}</p>
            </div>
        )
}

Poster.propTypes = {
    content: PropTypes.shape({
        title: PropTypes.string.isRequired,
        image: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired,
    }).isRequired
}
