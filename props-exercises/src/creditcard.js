import React from 'react';
import PropTypes from 'prop-types';

export var cardInfo = {
    cardholder: 'Bart van Enter',
    bank: 'Rabobank',
    expiration: '2012 / 02',
    number: '1234 5678 1234 5678'
}

export const CreditCard = ({info}) => {
    const {cardholder, bank, expiration, number} = info;
    return (
        <div className="card">
            <div className="bank">{bank}</div>
            <div className="number">{number}</div>
            <div className="code-expiration">
                <div className="code">1234</div>
                <div className="expiration">
                    <span className="expiration-label">Valid Thru</span>
                    <span className="expiration-date">{expiration}</span>
                </div>
            </div>
            <div className="cardholder">{cardholder}</div>
        </div>
    )
}

CreditCard.propTypes = {
    info: PropTypes.shape({
        cardholder: PropTypes.string.isRequired,
        bank: PropTypes.string.isRequired,
        expiration: PropTypes.string.isRequired,
        number: PropTypes.string.isRequired,
    }).isRequired
}