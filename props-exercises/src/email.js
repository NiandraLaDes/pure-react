import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

export var emailContent = {
    sender: 'Bart van Enter',
    subject: 'React excersise',
    date: '2018-07-25',
    message: 'This is an test email for the React excersises.'
}

export const Email = ({email, children}) => {
    const {sender, subject, date, message} = email;
    const dateString = moment(date).format('MMM Do');
    return (
        <div className="email">
            <div className="checkbox-icon">
                {children}
                <div><input type="checkbox" value="1"/></div>
                <div><i className="fa fa-map-pin pin-icon" aria-hidden="true"></i></div>
            </div>
            <div className="email-content">
                <div className="email-sender">{sender}</div>
                <div className="email-title">{subject}</div>
                <div className="email-date">{dateString}</div>
                <div className="email-message">{message}</div>
            </div>
        </div>
    )
}



Email.propTypes = {
    email: PropTypes.shape({
        sender: PropTypes.string.isRequired,
        subject: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired,
        message: PropTypes.string.isRequired,
    }).isRequired
}