import React from 'react';
import PropTypes from 'prop-types';

export const Tail = ({children, number}) => {
    let items = React.Children.toArray(children);
    let removed = items.splice(-number);
    return <div>{removed}</div>
}

export const Head = ({children, number}) => {
    let items = React.Children.toArray(children);
    let show = items.slice(0, number)
    return <div>{show}</div>
}

export const FirstChildOnly = ({children}) => {
    let items = React.Children.toArray(children);
    items.splice(1, items.length - 1);
    return <div>{items}</div>
}

export const LastChildOnly = ({children}) => {
    let items = React.Children.toArray(children);
    items.splice(0, items.length - 1);
    return <div>{items}</div>
}

export const Children = ({children}) => (
    <ul className="children">{React.Children.count(children)}</ul>
)

export const ErrorBox = ({children}) => (
    <div className="box-error">
        <i className="fas fa-exclamation-triangle"></i>{children}
    </div>
)

export const Nav = ({children}) => {
    let items = React.Children.toArray(children);
    for(let i = items.length - 1; i >= 1; i--) {
        items.splice(i, 0,
            <span key={i} className='separator'>|</span>
        );
    }

    return <div>{items}</div>
}

export const NavItem = ({children}) => (
    <span>{children}</span>
)

export const Dialog = ({children}) => (
    <main className="dialog">{children}</main>
)

export const Title = ({children}) => (
    <h1>{children}</h1>
)

export const Body = ({children}) => (
    <section>{children}</section>
)

export const Footer = ({children}) => (
    <footer>{children}</footer>
)

Children.propTypes = {
    children: PropTypes.element.isRequired
}

Tail.propTypes = {
    number: PropTypes.number,
    children: PropTypes.node
}

Dialog.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.node,
        PropTypes.string,
        PropTypes.number
    ])
}