import React from 'react';
import ReactDOM from 'react-dom';
import {Envelope, toPerson, fromPerson} from './envelope.js';
import {CreditCard, cardInfo} from './creditcard.js';
import {Poster, posterContent} from './poster.js';
import {Email, emailContent} from './email.js';
import {Children, Nav, NavItem, ErrorBox, FirstChildOnly, LastChildOnly, Head, Tail, Dialog, Title, Body, Footer} from './children.js';
import './index.css';

const Excersises = () => (
    <React.Fragment>
        <Nav>
            <NavItem url="/">Home</NavItem>
            <NavItem url="/about">About</NavItem>
            <NavItem url="/contact">Contact</NavItem>
        </Nav>
        <Envelope to={toPerson} from={fromPerson} stamp={10} />
        <CreditCard info={cardInfo} />
        <Poster content={posterContent} />
        <Email email={emailContent} />
        <Children>
            <React.Fragment>
                <h1>Children</h1>
                <p>Show me the children!</p>
                <h2>Subchildren</h2>
                <p>
                    Subchildren
                    <b>asdhasjgdfangsdfnasd</b>
                </p>
                <h2>Subchildren 2</h2>
            </React.Fragment>
        </Children>
        <ErrorBox>Something has gone wrong</ErrorBox>
        <FirstChildOnly>
            <div>Eerste div</div>
            <div>Tweede div</div>
            <div>Derde div</div>
            <div>Vierde div</div>
            <div>Vijfde div</div>
            <div>Zesde div</div>
            <div>Zevende div</div>
        </FirstChildOnly>
        <LastChildOnly>
            <div>Eerste div</div>
            <div>Tweede div</div>
            <div>Derde div</div>
        </LastChildOnly>
        <Head number={6}>
            <div>Eerste head</div>
            <div>Tweede head</div>
            <div>Derde head</div>
            <div>Vierde head</div>
            <div>Vijfde head</div>
            <div>Zesde head</div>
            <div>Zevende head</div>
        </Head>
        <Tail number={8}>
            <div>Eerste tail</div>
            <div>Tweede tail</div>
            <div>Derde tail</div>
            <div>Vierde tail</div>
            <div>Vijfde tail</div>
            <div>Zesde tail</div>
            <div>Zevende tail</div>
        </Tail>
        <Dialog>
            <Title>Titel</Title>
            <Body>Dit is de content van het hele verhaal</Body>
            <Footer>Close</Footer>
        </Dialog>
    </React.Fragment>
)

ReactDOM.render(
    <Excersises />,
    document.getElementById('root')
)