import React from 'react';
import PropTypes from 'prop-types';

const FileIcon = ({file}) => {
    let icon = 'fa-file-text-0';
    if(file.type === 'folder') {
        icon = 'fa-folder';
    }
    return (
        <span><i className={`fa ${icon}`}/></span>
    )
}

FileIcon.propTypes = {
    file: PropTypes.object.isRequired
}

export default FileIcon;
