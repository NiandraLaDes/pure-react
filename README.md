# Project Title

[Pure React Tutorials](https://daveceddia.com/pure-react/) by David Ceddia - An easy intro to React for beginners.

## Built With

* [React.js](https://reactjs.org/) - A JavaScript library for building user interfaces

## Authors

* **Bart van Enter** - *Initial work* - [Bitbucket](https://bitbucket.org/NiandraLaDes) and [Enter Webdevelopment](https://enter-webdevelopment.nl/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details