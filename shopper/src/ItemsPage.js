import React from 'react';
import PropTypes from 'prop-types';
import Item from './Item';

const ItemsPage = ({items, onAddToCart}) => {
    return (
        <React.Fragment>
            {items.length > 0 ? (
                <ul className="itempage-list">
                    {items.map(item => (
                        <li key={item.id}>
                            <Item item={item}>
                                <button className="item-addToCart" onClick={() => onAddToCart(item)}>
                                    Add to Cart
                                </button>
                            </Item>
                        </li>
                    ))}
                </ul>
            ) : (
                <div>No items found</div>
            )}
        </React.Fragment>
    )
}

ItemsPage.propTypes = {
    items: PropTypes.array.isRequired,
    onAddToCart: PropTypes.func.isRequired
}

export default ItemsPage;