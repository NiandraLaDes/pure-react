import React, {Component} from 'react';
import Nav from './Nav';
import ItemsPage from './ItemsPage';
import CartPage from './CartPage';
import {items} from './static-data';
import './App.css';

class App extends Component {

    state = {
        activeTab: 0,
        cart: [],
    }

    handleTabChange = (index) => {
        this.setState({
            activeTab: index
        })
    }

    handleAddToCart = (item) => {
        this.setState({
            cart: [...this.state.cart, item.id]
        })
    }

    handleRemoveFromCart = (item) => {
        let {cart} = this.state;
        let index = this.state.cart.indexOf(item.id);

        this.setState({
            cart: [
                ...cart.slice(0, index),
                ...cart.slice(index + 1)
            ]
        })
    }

    getCartInfo = () => {
        let {cart} = this.state;
        let itemCounts = cart.reduce((itemCounts, itemId) => {
            itemCounts[itemId] = itemCounts[itemId] || 0;
            itemCounts[itemId]++;
            return itemCounts;
        }, {});

        let cartItems = Object.keys(itemCounts).map(itemId => {
            var item = items.find(item =>
                item.id === parseInt(itemId, 10)
            );

            return {
                ...item,
                count: itemCounts[itemId]
            }
        })

        let cartTotal = Object.keys(cartItems).reduce((cartTotal, key) => {
            cartTotal += cartItems[key].price * cartItems[key].count;
            return cartTotal;
        }, 0);

        return {
            total: cartTotal,
            items: cartItems
        };
    }

    render() {
        let {activeTab, cart} = this.state;
        let cartInfo = this.getCartInfo();
        return (
            <div className="app">
                <Nav activeTab={activeTab} onTabChange={this.handleTabChange} cart={cart} cartInfo={cartInfo} />
                <main>
                    {activeTab === 0 ?
                        <ItemsPage items={items} onAddToCart={this.handleAddToCart}/> : activeTab === 1 ?
                            <CartPage onAddOne={this.handleAddToCart} onRemoveOne={this.handleRemoveFromCart} cartInfo={cartInfo}/> :
                            <ItemsPage items={items} onAddToCart={this.handleAddToCart}/>
                    }
                </main>
            </div>
        );
    }
}

export default App;
