import React from 'react';
import PropTypes from 'prop-types';
import Item from './Item';
import './CartPage.css';

const CartPage = ({onAddOne, onRemoveOne, cartInfo}) => {
    const {total, items} = cartInfo;
    return (
        <React.Fragment>
            {items.length > 0 ? (
                <React.Fragment>
                <ul className="cartpage-list">
                    {items.map(item => (
                        <li key={item.id} className="cartpage-item">
                            <Item item={item}>
                                <div className="cartitem-controls">
                                    <button className="cartitem-removeOne" onClick={() => onRemoveOne(item)}>&ndash;</button>
                                    <span className="cartitem-count">{item.count}</span>
                                    <button className="cartitem-addOne" onClick={() => onAddOne(item)}>+</button>
                                </div>
                            </Item>
                        </li>
                    ))}
                    <li className="carttotal">Total: ${total}</li>
                </ul>

                </React.Fragment>
            ) : (
                <div>Your cart is empty</div>
            )}
        </React.Fragment>
    )
}

CartPage.propTypes = {
    cartInfo: PropTypes.object.isRequired,
    onAddOne: PropTypes.func.isRequired,
    onRemoveOne: PropTypes.func.isRequired,
}

export default CartPage;