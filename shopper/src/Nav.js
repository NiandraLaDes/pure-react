import React from 'react';
import PropTypes from 'prop-types';

const Nav = ({onTabChange, activeTab, cartInfo, cart}) => {
    const {total} = cartInfo;
    return (
        <nav className="main-nav">
            <ul>
                <li className={`${activeTab === 0 && 'selected'}`}>
                    <NavLink index={0} onClick={onTabChange}>Items</NavLink>
                </li>
                <li className={`${activeTab === 1 && 'selected'}`}>
                    <NavLink index={1} onClick={onTabChange}>
                        Cart
                    </NavLink>
                </li>
                <li>
                        {cart.length > 0 &&
                            <NavLink index={1} onClick={onTabChange}>
                                <i className="fas fa-shopping-cart" /> {cart.length} {cart.length === 1 ? ' item' : 'items'} (${total})
                            </NavLink>
                        }
                </li>
            </ul>
        </nav>
    )
}

class NavLink extends React.Component {
    handleClick = () => {
        this.props.onClick(this.props.index)
    }

    render() {
        return (
            <a onClick={this.handleClick}>{this.props.children}</a>
        )
    }
}

Nav.propTypes = {
    activeTab: PropTypes.number.isRequired,
    onTabChange: PropTypes.func.isRequired,
    cartInfo: PropTypes.object.isRequired,
    cart: PropTypes.array.isRequired,
}

export default Nav;