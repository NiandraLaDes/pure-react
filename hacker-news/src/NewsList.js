import React from 'react';
import PropTypes from 'prop-types';
import NewsListItem from './NewsListItem.js';

const NewsList = ({newsItems}) => {
    if(newsItems.length > 0) {
        return (
            <table>
                <tbody>
                {newsItems.map(item => (
                    <NewsListItem key={item.id} item={item} />
                ))}
                </tbody>
            </table>
        )
    } else {
        return <div>No newsitems found.</div>
    }
}

NewsList.propTypes = {
    newsItems: PropTypes.array.isRequired
}

export default NewsList;