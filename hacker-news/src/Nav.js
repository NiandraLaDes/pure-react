import React from 'react';
import PropTypes from 'prop-types';

const Nav = ({children}) => {
    let items = React.Children.toArray(children);
    for(let i = items.length - 1; i > 1; i--) {
        items.splice(i, 0, <span className="separator">|</span>);
    }
    return <nav><ul>{items}</ul></nav>
}

Nav.propTypes = {
    children: PropTypes.node
}

const NavItem = ({children}) => (
    <li>{children}</li>
)

NavItem.propTypes = {
    children: PropTypes.node
}

export {Nav, NavItem};