import React from 'react';
import PropTypes from 'prop-types';
import Time from './Time.js';

const NewsItemPoints = ({points}) => {
    const {number, user, updated_at} = points;
    return (
        <span>{number} {number === 1 ? ' point' : 'points'} by {user} <Time time={updated_at} /></span>
    )
}

NewsItemPoints.propTypes = {
    points: PropTypes.object.isRequired
}

export default NewsItemPoints;