import React from 'react';
import ReactDOM from 'react-dom';
import NewsList from './NewsList.js';
import {Nav, NavItem} from './Nav.js';
import './index.css';

const App = () => (
    <React.Fragment>
        <header>
            <Nav>
                <NavItem url="">Hacker News</NavItem>
                <NavItem url="">new</NavItem>
                <NavItem url="">comments</NavItem>
                <NavItem url="">show</NavItem>
                <NavItem url="">ask</NavItem>
                <NavItem url="">jobs</NavItem>
                <NavItem url="">submit</NavItem>
            </Nav>
        </header>
        <section className="hacker-news">
            <NewsList newsItems={newsItems} />
        </section>
    </React.Fragment>
)

var newsItems = [
    {
        id: 1,
        title: 'Pyxel: A retro game development environment in Python',
        url: 'https://github.com/kitao/pyxel',
        site: 'github.com',
        points: {
            number: 298,
            user: '1wilkens',
            updated_at: '2018-07-18 21:24:00'
        },
        comments: 4
    },
    {
        id: 2,
        title: 'Pyxel: A retro game development environment in Python',
        url: 'https://github.com/kitao/pyxel',
        site: 'github.com',
        points: {
            number: 298,
            user: '1wilkens',
            updated_at: '2018-07-18 21:24:00'
        },
        comments: 4
    },
    {
        id: 3,
        title: 'Pyxel: A retro game development environment in Python',
        url: 'https://github.com/kitao/pyxel',
        site: 'github.com',
        points: {
            number: 298,
            user: '1wilkens',
            updated_at: '2018-07-18 21:24:00'
        },
        comments: 4
    },
    {
        id: 4,
        title: 'Pyxel: A retro game development environment in Python',
        url: 'https://github.com/kitao/pyxel',
        site: 'github.com',
        points: {
            number: 298,
            user: '1wilkens',
            updated_at: '2018-07-18 21:24:00'
        },
        comments: 4
    },
    {
        id: 5,
        title: 'Pyxel: A retro game development environment in Python',
        url: 'https://github.com/kitao/pyxel',
        site: 'github.com',
        points: {
            number: 298,
            user: '1wilkens',
            updated_at: '2018-07-18 21:24:00'
        },
        comments: 4
    }
]

ReactDOM.render(
    <App />,
    document.getElementById('root')
);
