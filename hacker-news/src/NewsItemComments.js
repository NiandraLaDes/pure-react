import React from 'react';
import PropTypes from 'prop-types';

const NewsItemComments = ({comments}) => (
    <span>{comments} {comments === 1 ? 'comment' : 'comments'}</span>
)

NewsItemComments.propTypes = {
    comments: PropTypes.number.isRequired
}

export default NewsItemComments;