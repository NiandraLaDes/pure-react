import React from 'react';
import PropTypes from 'prop-types';

const NewsTitleSite = ({item}) => (
    <div>
        <a href={item.url}>{item.title}</a> <a href={item.site}>({item.site})</a>
    </div>
)


NewsTitleSite.propTypes = {
    item: PropTypes.object.isRequired
}

export default NewsTitleSite;