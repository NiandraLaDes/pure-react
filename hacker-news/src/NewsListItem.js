import React from 'react';
import PropTypes from 'prop-types';
import NewsTitleSite from './NewsTitleSite.js';
import NewsItemPoints from './NewsItemPoints.js';
import NewsItemComments from './NewsItemComments.js';
import HideButton from './HideButton.js';


const NewsListItem = ({item}) => (
    <tr>
        <td>{item.id}</td>
        <td><i className="fa fa-caret-up" /></td>
        <td>
            <div className="news-item-header">
                <NewsTitleSite item={item}/>
            </div>
            <div className="news-item-footer">
                <NewsItemPoints points={item.points} /> | <HideButton /> | <NewsItemComments comments={item.comments} />
            </div>
        </td>
    </tr>
)

NewsListItem.propTypes = {
    item: PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired,
        site: PropTypes.string.isRequired,
        points: PropTypes.object.isRequired,
        comments: PropTypes.number.isRequired,
    }).isRequired
}

export default NewsListItem;