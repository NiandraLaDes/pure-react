import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Post from './Post';
import { json_posts } from './static-data';

class PostList extends Component {
    state = {
        posts: []
    }

    handleGoToUrl(url) {
        window.location = url;
    }

    handleAddVote(postId) {
        var key = Object.keys(this.state.posts).find(key => this.state.posts[key].id === postId);
        var posts = [...this.state.posts];
        posts[key].score++;
        this.setState({posts});
    }

    handleRemoveVote(postId) {
        var key = Object.keys(this.state.posts).find(key => this.state.posts[key].id === postId);
        var posts = [...this.state.posts];
        posts[key].score--;
        this.setState({posts});
    }

    sortPosts(posts) {
        let sorted = json_posts.data.children.sort( function(a, b) {
            return b.data.score - a.data.score || a.data.title.localeCompare(b.data.title);
        });

        let retVal = sorted.map(post => post.data);
        return retVal;
    }

    componentDidMount() {
        let posts = this.sortPosts(json_posts.data.children);
        this.setState({posts});
    }

    render() {
        if(this.state.posts.length > 0) {
            let posts = this.sortPosts(this.state.posts);
            return (
                    <ul className="postlist">
                    {posts.map(post => {
                        return (
                            <li key={post.id} className="postitem">
                                <Post
                                    post={post}
                                    addVote={this.handleAddVote.bind(this)}
                                    removeVote={this.handleRemoveVote.bind(this)}
                                    goToUrl={this.handleGoToUrl.bind(this)} />
                            </li>
                        )})}
                    </ul>
            )
        } else {
            return <div>No posts found.</div>
        }
    }
}

PostList.propTypes = {
    posts: PropTypes.array,
}

export default PostList;