import React from 'react';
import PropTypes from 'prop-types';
import Time from './Time';
import Votes from './Votes';
import Link from './Link';

const Post = ({post, addVote, removeVote, goToUrl}) => (
    <React.Fragment>
        <div className="postitem-votes">
            <Votes
                post={post}
                addVote={addVote}
                removeVote={removeVote}
            />
        </div>
        <div className="postitem-image">
            <img src={post.thumbnail !== 'self' ? post.thumbnail : 'http://i.imgur.com/sdO8tAw.png'} alt={post.title} />
        </div>
        <div className="postitem-content">
            <div className="postitem-title">
                <h2><Link onClick={() => goToUrl(post.url)}>{post.title}</Link></h2> <span>{post.domain}</span>
            </div>
            <div>Submitted <Time time={post.created_utc}/> by <Link>{post.author}</Link></div>
            <ul className="postitem-buttons">
                <li className="postitem-comments"><Link>{post.num_comments} {post.num_comments === 1 ? ' comment' : ' comments'}</Link></li>
                <li className="postitem-share"><Link>share</Link></li>
                <li className="postitem-save"><Link>save</Link></li>
                <li className="postitem-hide"><Link>hide</Link></li>
                <li className="postitem-report"><Link>report</Link></li>
                <li className="postitem-pocket"><Link>pocket</Link></li>
            </ul>
        </div>
    </React.Fragment>
)

Post.propTypes = {
    post: PropTypes.object.isRequired,
    addVote: PropTypes.func.isRequired,
    removeVote: PropTypes.func.isRequired,
    goToUrl: PropTypes.func.isRequired,
}

export default Post;