import React from 'react';
import PropTypes from 'prop-types';

const Votes = ({post, addVote, removeVote}) => (
    <React.Fragment>
        <div onClick={() => addVote(post.id)}><i className="fas fa-arrow-up"></i></div>
        <div>{post.score}</div>
        <div onClick={() => removeVote(post.id)}><i className="fas fa-arrow-down"></i></div>
    </React.Fragment>
)

Votes.propTypes = {
    post: PropTypes.object.isRequired,
    addVote: PropTypes.func.isRequired,
    // removeVote: PropTypes.func.isRequired
}

export default Votes;