import React, { Component } from 'react';
import { json_posts } from './static-data';
import PostList from './PostList';
import './App.css';

class App extends Component {
    state = {
        posts: []
    }

    handleGoToUrl(url) {
        window.location = url;
    }

    handleAddVote(postId) {
        var key = Object.keys(this.state.posts).find(key => this.state.posts[key].id === postId);
        var posts = [...this.state.posts];
        posts[key].score++;
        this.setState({posts});
    }

    handleRemoveVote(postId) {
        var key = Object.keys(this.state.posts).find(key => this.state.posts[key].id === postId);
        var posts = [...this.state.posts];
        posts[key].score--;
        this.setState({posts});
    }

    componentDidMount() {
        let sorted = json_posts.data.children.sort( function(a, b) {
            return b.data.score - a.data.score;
        });

        let posts = sorted.map(post => post.data);
        this.setState({posts});
    }

    render() {
        const {posts} = this.state;
        return (
            <React.Fragment>
                <main>
                    <PostList
                        posts={posts}
                        addVote={this.handleAddVote.bind(this)}
                        removeVote={this.handleRemoveVote.bind(this)}
                        goToUrl={this.handleGoToUrl}
                    />
                </main>
            </React.Fragment>
        )
    }
}

export default App;