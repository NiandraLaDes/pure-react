import React from 'react';
import PropTypes from 'prop-types';

const Link = ({children, onClick}) => (
    <a href={null} onClick={onClick}>{children}</a>
)

Link.propTypes = {
    children: PropTypes.node.isRequired
}

export default Link;