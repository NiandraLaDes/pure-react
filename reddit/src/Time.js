import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

const Time = ({time}) => {
    var date = moment.unix(time).format("YYYY-MM-DD HH:mm:ss");
    const timeString = moment(date).fromNow();
    return <span className="time">{timeString}</span>
}

Time.propTypes = {
    time: PropTypes.number.isRequired
}

export default Time;