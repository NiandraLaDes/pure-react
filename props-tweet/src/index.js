import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import moment from 'moment';
import './index.css'

var tweets = [
    {
        id: 1,
        message: "Something about cats.",
        gravatar: "xyz",
        author: {
            handle: "catperson",
            name: "NiandraLaDes"
        },
        likes: 3,
        retweets: 10,
        timestamp: "2016-07-30 21:24:37"
    },
    {
        id: 2,
        message: "Something about cats.",
        gravatar: "xyz",
        author: {
            handle: "catperson",
            name: "NiandraLaDes"
        },
        likes: 3,
        retweets: 10,
        timestamp: "2016-07-30 21:24:37"
    },
    {
        id: 3,
        message: "Something about cats.",
        gravatar: "xyz",
        author: {
            handle: "catperson",
            name: "NiandraLaDes"
        },
        likes: 3,
        retweets: 10,
        timestamp: "2016-07-30 21:24:37"
    }
];

const TweetList = ({tweets}) => {
    console.log(tweets.count);
    if(tweets.length > 0) {
        return (
            <ul className="tweet-list">
                {tweets.map(tweet => (
                    <Tweet key={tweet.id} tweet={tweet} />
                ))}
            </ul>
        )
    } else {
        return 'No Tweets';
    }
}

function Tweet({tweet}) {
    return (
        <li className="tweet">
            <Avatar hash={tweet.gravatar}/>
            <div className="content">
                <NameWithHandle author={tweet.author}/> <Time time={tweet.timestamp}/>
                <Message text={tweet.message}/>
                <div className="buttons">
                    <ReplyButton />
                    <RetweetButton count={tweet.retweets}/>
                    <LikeButton count={tweet.likes}/>
                    <MoreOptionsButton />
                </div>
            </div>
        </li>
    )
}

function Avatar({hash}) {
    var url = `https://www.gravatar.com/avatar/${hash}`;
    return (
        <img
            src={url}
            className="avatar"
            alt="avatar"
        />
    )
}

function Message({text}) {
    return (
        <div className="message">{text}</div>
    )
}

function NameWithHandle({author}) {
    const {name, handle} = author;
    return (
        <span className="name-with-handle">
            <span className="name">{name}</span>
            <span className="handle">@{handle}</span>
        </span>
    )
}

function Count({count, name}) {
    if(count > 0) {
        let elementId = `${name}-count`;
        return (
            <span className={elementId}>
                {count}
            </span>
        )
    } else {
        return null;
    }
}

const Time = ({time}) => {
    const timeString = moment(time).fromNow();
    return <span className="time">{timeString}</span>
}

const ReplyButton = () => (
    <i className="fa fa-reply reply-button" />
)

const RetweetButton = ({count}) => (
    <span className="retweet-button">
        <i className="fa fa-retweet" />
        <Count count={count}  name="retweet" />
    </span>
)

const LikeButton = ({count}) => (
    <span className="like-button">
        <i className="fa fa-heart" />
        <Count count={count} name="like" />
    </span>
)

LikeButton.propTypes = {
    count: PropTypes.number
}

RetweetButton.proptypes = {
    count: PropTypes.number
}

Message.propTypes = {
    text: PropTypes.string
}

Time.propTypes = {
    time: PropTypes.string
}

NameWithHandle.propTypes = {
    author: PropTypes.shape({
        name: PropTypes.string.isRequired,
        handle: PropTypes.string.isRequired
    }).isRequired
}

Avatar.propTypes = {
    hash: PropTypes.string
}

Tweet.propTypes = {
    tweet: PropTypes.object.isRequired
}

TweetList.propTypes = {
    tweets: PropTypes.array.isRequired
}
ReactDOM.render(
    <TweetList tweets={tweets}/>,
    document.getElementById('root')
)