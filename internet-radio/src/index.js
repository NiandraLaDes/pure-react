import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import './index.css';

const Radio = ({genres}) => {
    return (
        <section>
            <h2>Genres</h2>
            <div>
                {genres.map((genre, index) => {
                    let size = 'btn-xs';
                    let counter = 29;
                    if(index > counter) {
                        size = 'btn-sm';
                    }
                    if(index > (counter * 2)) {
                        size = 'btn-md';
                    }
                    if(index > (counter * 3)) {
                        size = 'btn-lg';
                    }
                    return <a key={index} href="" className={`btn btn-default text-capitalize ${size}`}>{genre}</a>
                })}
            </div>
        </section>
    )
}

Radio.propTypes = {
    genres: PropTypes.array.isRequired
}

var genres = [
    'Blues', 'Japananese', 'Rock', 'Metal', 'Electro', 'House', 'Soul', '50s', 'Chillout',
    'Blues', 'Japananese', 'Rock', 'Metal', 'Electro', 'House', 'Soul', '50s', 'Chillout',
    'Blues', 'Japananese', 'Rock', 'Metal', 'Electro', 'House', 'Soul', '50s', 'Chillout',
    'Blues', 'Japananese', 'Rock', 'Metal', 'Electro', 'House', 'Soul', '50s', 'Chillout',
    'Blues', 'Japananese', 'Rock', 'Metal', 'Electro', 'House', 'Soul', '50s', 'Chillout',
    'Blues', 'Japananese', 'Rock', 'Metal', 'Electro', 'House', 'Soul', '50s', 'Chillout',
    'Blues', 'Japananese', 'Rock', 'Metal', 'Electro', 'House', 'Soul', '50s', 'Chillout',
    'Blues', 'Japananese', 'Rock', 'Metal', 'Electro', 'House', 'Soul', '50s', 'Chillout',
    'Blues', 'Japananese', 'Rock', 'Metal', 'Electro', 'House', 'Soul', '50s', 'Chillout',
    'Blues', 'Japananese', 'Rock', 'Metal', 'Electro', 'House', 'Soul', '50s', 'Chillout',
    'Blues', 'Japananese', 'Rock', 'Metal', 'Electro', 'House', 'Soul', '50s', 'Chillout',
    'Blues', 'Japananese', 'Rock', 'Metal', 'Electro', 'House', 'Soul', '50s', 'Chillout',
    'Blues', 'Japananese', 'Rock', 'Metal', 'Electro', 'House', 'Soul', '50s', 'Chillout',
    'Blues', 'Japananese', 'Rock', 'Metal', 'Electro', 'House', 'Soul', '50s', 'Chillout',
    'Blues', 'Japananese', 'Rock', 'Metal', 'Electro', 'House', 'Soul', '50s', 'Chillout',
]

ReactDOM.render(<Radio genres={genres}/>, document.getElementById('root'));
