import React from 'react';
import ReactDOM from 'react-dom';
import CardLists from './CardLists.js';
import './index.css';

var cardLists =[
    {
        id: 1,
        name: "Phone Features",
        cards: [
                {
                    id: 1,
                    name: "Card 1"
                },
                {
                    id: 2,
                    name: "Card 2"
                },
                {
                    id: 3,
                    name: "Card 3"
                },
                {
                    id: 4,
                    name: "Card 4"
                },
                {
                    id: 5,
                    name: "Card 5"
                }
        ]
    },
    {
        id: 2,
        name: "TV Features",
        cards: [
            {
                id: 1,
                name: "Card 1"
            },
            {
                id: 2,
                name: "Card 2"
            },
            {
                id: 3,
                name: "Card 3"
            },
        ]
    },
    {
        id: 3,
        name: "Car Features",
        cards: [
            {
                id: 1,
                name: "Card 1"
            },
            {
                id: 2,
                name: "Card 2"
            },
            {
                id: 3,
                name: "Card 3"
            },
            {
                id: 4,
                name: "Card 4"
            },
        ]
    },
    {
        id: 4,
        name: "Sports",
        cards: [
            {
                id: 1,
                name: "Card 1"
            },
            {
                id: 2,
                name: "Card 2"
            },
            {
                id: 3,
                name: "Card 3"
            },
            {
                id: 4,
                name: "Card 4"
            },
        ]
    },
]


ReactDOM.render(
    <CardLists cardLists={cardLists}/>,
    document.getElementById('root')
);
