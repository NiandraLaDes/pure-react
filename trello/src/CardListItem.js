import React from 'react';
import PropTypes from 'prop-types';

const CardListItem = ({card}) => (
    <li key={card.id} className="card-list-item">{card.name}</li>
)

CardListItem.propTypes = {
    card: PropTypes.object.isRequired
}

export default CardListItem;
