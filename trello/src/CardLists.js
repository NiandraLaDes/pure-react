import React from 'react';
import PropTypes from 'prop-types';
import CardList from './CardList.js';

const CardLists = ({cardLists}) => {
    return (
        <div className="card-lists">
            {cardLists.length > 0 &&
                <ul>
                    {cardLists.map(list => (
                        <CardList key={list.id} cardList={list}/>
                    ))}
                </ul>
            }
        </div>
    )
}

CardLists.propTypes = {
    cardLists: PropTypes.array.isRequired
}

export default CardLists;