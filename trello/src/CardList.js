import React from 'react';
import PropTypes from 'prop-types';
import CardListName from './CardListName.js';
import MoreOptionsButton from './MoreOptionsButton.js';
import AddCardButton from './AddCardButton.js';
import CardListItem from './CardListItem.js';

const CardList = ({cardList}) => {
    return (
        <li className="card-list">
            <header>
                <CardListName name={cardList.name}/> <MoreOptionsButton />
            </header>
            {cardList.cards.length > 0 &&
                <ul>
                    {cardList.cards.map(card => (
                        <CardListItem key={card.id} card={card} />
                    ))}
                </ul>
            }
            <footer>
                <AddCardButton />
            </footer>
        </li>
    )
}

CardList.propTypes = {
    cardList: PropTypes.object.isRequired
}

export default CardList;