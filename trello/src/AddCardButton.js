import React from 'react';

const AddCardButton = () => (
    <div className="add-card">Add a Card...</div>
)

export default AddCardButton;