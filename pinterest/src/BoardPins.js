import React from 'react';
import PropTypes from 'prop-types';

const BoardPins = ({pins}) => (
    <div className="board-pins">
        {pins} {pins === 1 ? 'pin' : 'pins'}
    </div>
)

BoardPins.propTypes = {
    pins: PropTypes.number.isRequired
}

export default BoardPins;