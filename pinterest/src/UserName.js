import React from 'react';
import PropTypes from 'prop-types';

const UserName = ({username}) => (
    <h1>{username}</h1>
)

UserName.propTypes = {
    username: PropTypes.string.isRequired
}

export default UserName;