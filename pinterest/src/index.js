import React from 'react';
import ReactDOM from 'react-dom';
import UserLogo from './UserLogo.js';
import UserName from './UserName.js';
import {Nav, NavItem} from './Nav.js';
import BoardList from './BoardList.js';
import './index.css';

const Pinterest = ({user}) => {
    var userUrl = `https://nl.pinterest.com/${user.page}`
    return (
        <React.Fragment>
            <header>
                <UserLogo user={user} />
                <UserName username={user.username} />
                <Nav>
                    <NavItem url={`${userUrl}/boards`}><div>{user.boards.length}</div> {user.boards.length === 1 ? 'board' : 'boards'}</NavItem>
                    <NavItem url={`${userUrl}/pins`}><div>{user.pins}</div> {user.pins === 1 ? 'pin' : 'pins'}</NavItem>
                    <NavItem url={`${userUrl}/likes`}><div>{user.likes}</div> {user.likes === 1 ? 'like' : 'likes'}</NavItem>
                    <NavItem url={`${userUrl}/followers`}><div>{user.followers}</div> {user.followers === 1 ? 'follower' : 'followers'}</NavItem>
                    <NavItem url={`${userUrl}/following`}><div>{user.following}</div> following</NavItem>
                </Nav>
            </header>
            <main>
                <BoardList boards={user.boards} />
            </main>
        </React.Fragment>
    )
}

var user = {
    id: 1,
    page: 'aviationhd',
    url: 'www.removeandreplace.com',
    logo: 'https://i.pinimg.com/280x280_RS/7b/eb/40/7beb401620fabf62dc1841484eb47844.jpg',
    username: 'Aviation Explorer',
    followers: 1.555,
    following: 1.998,
    pins: 1.998,
    likes: 1.998,
    boards: [
        {
            id: 1,
            title: 'Exceptional Aviation',
            img: 'https://i.pinimg.com/550x/37/65/53/376553828e18f2d4ae8ddd70d8539c37.jpg',
            pins: 214
        },
        {
            id: 2,
            title: 'Aviation',
            img: 'https://i.pinimg.com/550x/37/65/53/376553828e18f2d4ae8ddd70d8539c37.jpg',
            pins: 1
        },
        {
            id: 4,
            title: 'Aviation',
            img: 'https://i.pinimg.com/550x/37/65/53/376553828e18f2d4ae8ddd70d8539c37.jpg',
            pins: 607
        },
        {
            id: 5,
            title: 'Aviation',
            img: 'https://i.pinimg.com/550x/37/65/53/376553828e18f2d4ae8ddd70d8539c37.jpg',
            pins: 607
        },
        {
            id: 11,
            title: 'Exceptional Aviation',
            img: 'https://i.pinimg.com/550x/37/65/53/376553828e18f2d4ae8ddd70d8539c37.jpg',
            pins: 214
        },
        {
            id: 12,
            title: 'Aviation',
            img: 'https://i.pinimg.com/550x/37/65/53/376553828e18f2d4ae8ddd70d8539c37.jpg',
            pins: 1
        },
        {
            id: 14,
            title: 'Aviation',
            img: 'https://i.pinimg.com/550x/37/65/53/376553828e18f2d4ae8ddd70d8539c37.jpg',
            pins: 607
        },
        {
            id: 15,
            title: 'Aviation',
            img: 'https://i.pinimg.com/550x/37/65/53/376553828e18f2d4ae8ddd70d8539c37.jpg',
            pins: 607
        }
    ]
}


ReactDOM.render(<Pinterest user={user}/>, document.getElementById('root'));
