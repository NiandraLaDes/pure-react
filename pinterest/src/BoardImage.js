import React from 'react';
import PropTypes from 'prop-types';

const BoardImage = ({board}) => (
    <div className="board-image">
        <img src={board.img} alt={board.title}/>
    </div>
)

BoardImage.propTypes = {
    board: PropTypes.object.isRequired
}

export default BoardImage;