import React from 'react';
import PropTypes from 'prop-types';
import Board from './Board.js';

const BoardList = ({boards}) => {
    if(boards.length > 0) {
        return (
            <ul className="board-list">
                {boards.map(board => (
                    <Board key={board.id} board={board} />
                ))}
            </ul>
        )
    } else {
        return 'No boards found.';
    }
}

BoardList.propTypes = {
    boards: PropTypes.array.isRequired
}

export default BoardList;