import React from 'react';
import PropTypes from 'prop-types';

const BoardTitle = ({title}) => (
    <h2>{title}</h2>
)

BoardTitle.propTypes = {
    title: PropTypes.string.isRequired
}

export default BoardTitle;