import React from 'react';
import PropTypes from 'prop-types';
import BoardImage from './BoardImage.js';
import BoardTitle from './BoardTitle.js';
import BoardPins from './BoardPins.js';
import FollowButton from './FollowButton.js';


const Board = ({board}) => (
    <li className="board">
        <BoardImage board={board} />
        <div className="board-title-follow">
            <div className="board-title-pins">
                <BoardTitle title={board.title} />
                <BoardPins pins={board.pins} />
            </div>
            <FollowButton />
        </div>
    </li>
)

Board.propTypes = {
    board: PropTypes.object.isRequired
}

export default Board;