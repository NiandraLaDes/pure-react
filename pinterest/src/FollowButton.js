import React from 'react';

const FollowButton = () => (
    <div className="follow-btn">
        <input type="button" value="Follow" />
    </div>
)

export default FollowButton;