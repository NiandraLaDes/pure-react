import React from 'react';
import PropTypes from 'prop-types';

const UserLogo = ({user}) => {
    var url = `https://nl.pinterest.com/${user.pagename}`;
    return (
        <a href={url}>
            <img className="logo" src={user.logo} alt={user.username} />
        </a>
    )
}

UserLogo.propTypes = {
    user: PropTypes.object.isRequired
}

export default UserLogo;