import React from 'react';
import PropTypes from 'prop-types';

const Nav = ({children}) => (
    <nav>
        <ul>{children}</ul>
    </nav>
)

Nav.propTypes = {
    children: PropTypes.node
}

const NavItem = ({url, children}) => (
    <li><a href={url}>{children}</a></li>
)

NavItem.propTypes = {
    children: PropTypes.node.isRequired,
    url: PropTypes.string.isRequired
}

export {Nav, NavItem}